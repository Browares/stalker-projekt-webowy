package paczka.stalker;

import java.sql.Timestamp;

public class Discovery {
	
	private long id;
	private String name;
	private String description;
	private String url;
	private Timestamp timestamp;
	private User user;
	private int upVote;
	private int downVote;
	
	public Discovery() {}
	
	public Discovery (Discovery discovery) {
	this.id = discovery.id;
	this.name = discovery.name;
	this.description = discovery.description;
	this.url = discovery.url;
	this.timestamp = new Timestamp (discovery.timestamp.getTime());
	this.user = new User(discovery.user);
	this.upVote = discovery.upVote;
	this.downVote = discovery.downVote;
	}
}
