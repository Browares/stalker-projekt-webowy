package paczka.stalker.dao;

import paczka.stalker.Vote;

public interface VoteDAO extends GenericDAO<Vote, Long> {
	 
    public Vote getVoteByUserIdDiscoveryId(long userId, long discoveryId);
}