package paczka.stalker.dao;

import java.util.List;

import paczka.stalker.Discovery;

public interface DiscoveryDAO extends GenericDAO <Discovery, Long> {

	List<Discovery> getAll();
}
