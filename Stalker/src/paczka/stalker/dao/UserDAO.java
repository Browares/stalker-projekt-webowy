package paczka.stalker.dao;


import java.util.List;
 
import paczka.stalker.*;
 
 
public interface UserDAO extends GenericDAO<User, Long> {
 
    List<User> getAll();
    User getUserByUsername(String username);
     
}